app.controller('InternsController', function ($scope) {

            $scope.interns = [
                {Id:'01', name:'Gagan', college:'CCET', department:'Front-end', joining_date:'01/01/2015', cgpa:'7.5' },
                {Id:'02', name:'Navit', college:'CCET', department:'Front-end', joining_date:'01/02/2015', cgpa:'8.5' },
                {Id:'03', name:'Prabhdeep',college:'CCET',department:'IOS', joining_date:'01/01/2015', cgpa:'7.5' },
                {Id:'04', name:'Ramita', college:'UIET', department:'Back-end', joining_date:'01/03/2015', cgpa:'6.8' },
                {Id:'05', name:'Renuka', college:'CCET', department:'Front-end', joining_date:'01/01/2015', cgpa:'7.2' },
                {Id:'06', name:'Shubam', college:'CCET', department:'Front-end', joining_date:'01/05/2015', cgpa:'7.5' },
                {Id:'07', name:'Jaskirat',college:'CCET',department:'IOS', joining_date:'01/02/2015', cgpa:'8.4' },
                {Id:'08', name:'laxmi', college:'CCET', department:'Android', joining_date:'01/01/2015', cgpa:'9.0' },
                {Id:'09', name:'Vikas', college:'UIET', department:'Front-end', joining_date:'01/02/2015', cgpa:'6.5' },
                {Id:'10', name:'Mehak', college:'UIET', department:'Front-end', joining_date:'01/03/2015', cgpa:'7.1' }
                ];
});
               