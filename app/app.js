(function (window) {
    var app = angular.module('myApp', ['ngRoute']);
    app.config(['$routeProvider',
        function ($routeProvider) {
            $routeProvider.
                when('/', {
                    title: 'Dashboard',
                    templateUrl: 'app/internsDashboard/angularAdminPanel.html',
                    controller: 'InternsController'
                })
                .when('/salary', {
                    title: 'salary',
                    templateUrl: 'app/salary/salary.html',
                    controller: 'salaryController'
                })
                .otherwise({
                    redirectTo: '/login'
                });

        }]);
    window.app = app;
}(window));




 
